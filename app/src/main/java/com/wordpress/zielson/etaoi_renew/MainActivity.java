package com.wordpress.zielson.etaoi_renew;

import android.app.Activity;
import android.content.ClipData;
import android.inputmethodservice.InputMethodService;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zielson on 2016-10-18.
 * @author Kamil Zieliński
 * kamil.zielinski.1992@gmail.com
 * https://zielware.wordpress.com/2016/10/20/lets-create-new-keyboard-for-android-devices-etaoi-keyboard-implementation-tutorial-dragndropp-part-1/
 */

public class MainActivity extends Activity {

    //we have 5 buttons with letters..
    private List<Button> buttons;

    //textView is area where we can see our typing
    private TextView textView;

    //this is buffor for content to display
    private String text = "";

    //aray which contains id`s of buttons (see layout.xml)
    private static final int[] LETTER_BUTTONS = {
            R.id.ABCDE,
            R.id.FGHIJ,
            R.id.KLMNO,
            R.id.PQRST,
            R.id.UVWYZ,
    };

    //it should be overriden
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        //textView should point to textView from layout.xml
        textView = (TextView)findViewById(R.id.textView);

        //we have a few objects of the same type, so array (list) is recommended
        buttons = new ArrayList<Button>();

        initialize_Letter_Buttons(LETTER_BUTTONS);
        //initially and after drag`n`drop operation, we should do this
        setColors();
        setLabels();
    }

    private void initialize_Letter_Buttons(int[] letterButtons){
        for (int id : letterButtons){
            //button should point to appropriate button from layout.xml
            Button button = (Button)findViewById(id);

            //listener for buttons
            button.setOnTouchListener(new MyTouchListener());

            //add buttons to an arrau
            buttons.add(button);
        }
    }

    private class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            //when user pressing down on button..
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                //after drag`n`drop operation, we should do this
                setLabelsWhenTouched(view);
                //when button is touched and pressed down, it should be 'draggable'
                setButtonsDragListener(view);
                setupDrag(view);
                return true;
            }else{
                return false;
            }
        }
    }

    //when button is touched
    //labels on buttons should contain single letters
    private void setLabelsWhenTouched(View view) {

        //we can intercept letter from buttons 'view'
        String butxt = ((Button) view).getText() + "";

        for(int i = 0; i < 5; i++)
            try {
                buttons.get(i).setText(butxt.substring(i, i + 1));
            } catch (IndexOutOfBoundsException e) {
                buttons.get(i).setText("");
            }
    }

    private void setupDrag(View view){
        ClipData data = ClipData.newPlainText("", "");
        //shadow for button
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        //start dragging the item touched
        view.startDrag(data, shadowBuilder, view, 0);
    }

    //we already have touchlistener, so now it is time for drag listener!
    private void setButtonsDragListener(View view){
        for(int i = 0; i < 5; i++)
            buttons.get(i).setOnDragListener(new LetterDragListener(buttons.get(i).getText() + ""));
    }

    private View dragOverButton;

    public class LetterDragListener implements View.OnDragListener{
        public String currentLetter = "";

        //constructor
        //getting label (it means letters) from button
        LetterDragListener(String string ){
            currentLetter=string;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                //when button enter into area of other buttons...
                case DragEvent.ACTION_DRAG_ENTERED:
                    //change the shape of the view
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    dragOverButton = v;
                    break;
                //and when the button exit from area of other buttons...
                case DragEvent.ACTION_DRAG_EXITED:
                    //change the shape of the view back to normal
                    setColors();
                    if (dragOverButton != v)
                        //change the shape of the view
                        dragOverButton.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                //when button is dropped
                case DragEvent.ACTION_DROP:
                    text += currentLetter ;
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    //initially and after drag`n`drop operation, we should do this
                    setLabels();
                    setColors();
                    break;
                default:
                    break;
            }

            textView.setText(text);
            return true;
        }
    }

    public void setColors(){
        //we have colors defined in res/values/colors.xml
        buttons.get(0).setBackgroundColor(getResources().getColor(R.color.blue));
        buttons.get(1).setBackgroundColor(getResources().getColor(R.color.green));
        buttons.get(2).setBackgroundColor(getResources().getColor(R.color.yellow));
        buttons.get(3).setBackgroundColor(getResources().getColor(R.color.orange));
        buttons.get(4).setBackgroundColor(getResources().getColor(R.color.red));
    }

    public void setLabels() {
        buttons.get(0).setText("ABCDE");
        buttons.get(1).setText("FGHIJ");
        buttons.get(2).setText("KLMNO");
        buttons.get(3).setText("PQRST");
        buttons.get(4).setText("UVWYZ");
    }
}
